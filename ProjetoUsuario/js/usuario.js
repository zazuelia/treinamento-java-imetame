$(document).ready(function(){

    carregaUsuarios();

    function carregaUsuarios() {

        console.log("click");
        var data = {
            serviceName: "consultar-usuario-ativo",
            userName: localStorage.getItem("userName")
        }

        $.ajax({
            url: "http://localhost:8080/ProjetoImetame/resources/usuarios/consultar-usuario-ativo",
            method: "POST",
            contentType: "application/json",
            data: JSON.stringify(data),
            dataType: "json",
            beforeSend: function(xhr){
                xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("token"));
            }
        }).done(function(resposta, status, jqXHR){
            console.log(resposta.Message);
            loadAccordionUsuario(JSON.parse(resposta.Message));
        }).fail(function( jqXHR, textStatus ) {
            
            exibirMensagem("Erro", jqXHR.responseText, true);
        });        
    };

    function limpaUsuarios(){
        $("#accordionUsuario").html("");
    }

    function exibirMensagem(titulo, mensagem, redirecionaLogin){

        $(".modal-title").html(titulo);
        $(".modal-body").html(mensagem);
        $("#messageModal").modal("show");

        $("#messageModal").on("hidden.bs.modal", function(){

            if(redirecionaLogin){
                window.location = "index.html";
            }
        });
    }

    $("#btnSair").click(function(){

        window.location = "index.html";
    });

    function loadAccordionUsuario(params){

        dados = [];
        dados = Object.values(params);
        console.log(params);
        var html = "";

        if(Array.isArray(params))
        {
            dados.map(usuario => {

                var heading = `${usuario.id}_${usuario.usuario}`;
                var collapse = usuario.usuario;

                html += `<div class="card">
                <div class="card-header" id="${heading}">
                    <h2 class="mb-0">
                        <button class="btn btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#${collapse}" aria-expanded="false" aria-controls="${collapse}">
                        ${usuario.name}
                        </button>
                    </h2>                              
                </div>
            
                <div id="${collapse}" class="collapse" aria-labelledby="${heading}" data-parent="#accordionUsuario">
                <div class="card-body">                    

                    <form id="editForm_${usuario.usuario}">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                            <label for="inputUserEdit_${usuario.usuario}">Usuario</label>
                            <input type="text" class="form-control" id="inputUserEdit_${usuario.usuario}" value="${usuario.usuario}">
                            </div>
                            <div class="form-group col-md-6">
                            <label for="inputNameEdit_${usuario.usuario}">Nome Completo</label>
                            <input type="text" class="form-control" id="inputNameEdit_${usuario.usuario}" value="${usuario.name}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                            <label for="inputEmail4Edit_${usuario.usuario}">Email</label>
                            <input type="email" class="form-control" id="inputEmail4Edit_${usuario.usuario}" value="${usuario.email}">
                            </div>
                            <div class="form-group col-md-6">
                            <label for="inputPassword4Edit_${usuario.usuario}">Senha</label>
                            <input type="password" class="form-control" id="inputPassword4Edit_${usuario.usuario}" value="${usuario.password}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputAddressEdit_${usuario.usuario}">Endereço</label>
                            <input type="text" class="form-control" id="inputAddressEdit_${usuario.usuario}" placeholder="Rodovia do Sol" value="${usuario.endereco}">
                        </div>
                        <div class="form-group">
                            <label for="inputAddress2Edit_${usuario.usuario}">Complemento</label>
                            <input type="text" class="form-control" id="inputAddress2Edit_${usuario.usuario}" placeholder="Apartmento" value="${usuario.complemento}">
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                            <label for="inputCityEdit_${usuario.usuario}">Cidade</label>
                            <input type="text" class="form-control" id="inputCityEdit_${usuario.usuario}" value="${usuario.cidade}">
                            </div>
                            <div class="form-group col-md-6">
                            <label for="inputStateEdit_${usuario.usuario}">Estado</label>
                            <select id="inputStateEdit_${usuario.usuario}" class="form-control">
                                <option>Selecione...</option>
                                <option ${(usuario.estado === 'ES') ? 'SELECTED' : ''}>ES</option>
                                <option ${(usuario.estado === 'SP') ? 'SELECTED' : ''}>SP</option>
                                <option ${(usuario.estado === 'RJ') ? 'SELECTED' : ''}>RJ</option>
                                <option ${(usuario.estado === 'BA') ? 'SELECTED' : ''}>BA</option>                        
                            </select>
                            </div>
                            <div class="form-group col-md-4">
                            <label for="inputZipEdit_${usuario.usuario}">CEP</label>
                            <input type="text" class="form-control" id="inputZipEdit_${usuario.usuario}" value="${usuario.cep}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="gridCheckEdit_${usuario.usuario}" ${(usuario.ativo === 0) ? '' : 'CHECKED'}>
                            <label class="form-check-label" for="gridCheckEdit_${usuario.usuario}">
                                Ativo
                            </label>
                            </div>
                        </div>
                    
                    </form> 

                    <button name="apagar" class="btn btn-outline-danger" id="btnApagar_${heading}">
                        Apagar
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                            <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                        </svg>
                    </button>
                    <button name="editar" class="btn btn-outline-success" id="btnEditar_${heading}">
                        Salvar
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                        </svg>
                    </button>
                    <button name="pdf" class="btn btn-outline-secondary" id="btnPDF_${heading}">
                        PDF
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-file-earmark-text-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M2 2a2 2 0 0 1 2-2h5.293A1 1 0 0 1 10 .293L13.707 4a1 1 0 0 1 .293.707V14a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm7.5 1.5v-2l3 3h-2a1 1 0 0 1-1-1zM4.5 8a.5.5 0 0 0 0 1h7a.5.5 0 0 0 0-1h-7zM4 10.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm0 2a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5z"/>
                        </svg>
                    </button>
                </div>
                </div>
            </div>`;          
          
            });
        } else {
            
                var heading = `${params.id}_${params.usuario}`;
                var collapse = params.usuario;

                html += `<div class="card">
                <div class="card-header" id="${heading}">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#${collapse}" aria-expanded="false" aria-controls="${collapse}">
                    ${params.name}
                    </button>
                </h2>
                </div>
            
                <div id="${collapse}" class="collapse" aria-labelledby="${heading}" data-parent="#accordionUsuario">
                <div class="card-body">
                    <p>${params.email}</p>
                    <p>${params.telefone}</p>
                    <p>${params.ativo}</p>
                </div>
                </div>
            </div>`; 
        }

        $("#accordionUsuario").append(html);        
        
    }

    $("#btnAdicionar").click(function(){
        $("#addModal").modal("show");
    });

    $("#addForm").submit(function(event){

        event.preventDefault();
        var dados = {
            userName: "jelia",
            id: 0,
            serviceName: "incluir-aterar-usuario",
            email: $("#inputEmail4").val(),
            password: CryptoJS.SHA256($("#inputPassword4").val()).toString(),
            endereco: $("#inputAddress").val(),
            complemento: $("inputAddress2").val(),
            cidade: $("#inputCity").val(),
            estado: $("#inputState option:selected").val(),
            cep: $("#inputZip").val(),
            ativo: $("#gridCheck").prop("checked") ? 1 : 0,
            user_name: $("#inputUser").val(),
            name: $("#inputName").val()
        }

        $.ajax({
            url: $(this).attr("action"),
            method: $(this).attr("method"),
            type: "json",
            data: JSON.stringify(dados),
            contentType: "application/json",
            beforeSend: function(xhr){
                xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("token"));
            }
        }).done(function(resposta, status, jqXhr){

            console.log(resposta.Message);
            loadAccordionUsuario(JSON.parse(resposta.Message));
            $("#addModal").modal("hide");

        }).fail(function(jqXhr, status){

            exibirMensagem("Erro", jqXhr.responseText, (jqXHR.status === 401) ? true : false);
        });        
    });

    $("body").on("click", "button[name='apagar']",function(){        

        var id = $(this).attr("id").split("_")[1];
        var dados = {
            userName: localStorage.getItem("userName"),
            serviceName: "apagar-usuario",
            id: id
        };

        $.ajax({
            url: "http://localhost:8080/ProjetoImetame/resources/usuarios/apagar-usuario/id",
            type: "json",
            method: "POST",
            data: JSON.stringify(dados),
            contentType: "application/json",
            beforeSend: function(xhr){
                xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("token"));
            }
        }).done(function(){
            limpaUsuarios();
            carregaUsuarios();
        }).fail(function(jqXhr, status){
            exibirMensagem("Erro", jqXhr.responseText, (jqXhr.status === 401) ? true : false);
        });
        
    });

    $("body").on("click", "button[name='editar']",function(){

        var userName = $(this).siblings("form").attr("id").split("_")[1];
        var id = $(this).attr("id").split("_")[1];
        var dados = {
            userName: localStorage.getItem("userName"),
            serviceName: "incluir-alterar-usuario",
            id: id,
            user_name: $("#inputUserEdit_" + userName).val(),
            name: $("#inputNameEdit_" + userName).val(),
            email: $("#inputEmail4Edit_" + userName).val(),
            password: $("#inputPassword4Edit_" + userName).val(),
            endereco: $("#inputAddressEdit_" + userName).val(),
            complemento: $("#inputAddress2Edit_" + userName).val(),
            cidade: $("#inputCityEdit_" + userName).val(),
            estado: $("#inputStateEdit_" + userName + " option:selected").val(),
            cep: $("#inputZipEdit_" + userName).val(),
            ativo: $("gridCheckEdit_" + userName).prop("checked") ? 1 : 0

        };

        $.ajax({
            url: "http://localhost:8080/ProjetoImetame/resources/usuarios/incluir-alterar-usuario",
            type: "json",
            method: "POST",
            data: JSON.stringify(dados),
            contentType: "application/json",
            beforeSend: function(xhr){
                xhr.setRequestHeader("Authorization", "Bearer " + localStorage.getItem("token"));
            }
        }).done(function(){
            $('.toast-header').html("Editar Usuário");
            $('.toast-body').html("Usuário alterado com sucesso!")
            $('.toast').toast('show');
        }).fail(function(){
            exibirMensagem("Erro", jqXhr.responseText, (jqXHR.status === 401) ? true : false);
        });
    });

    $("body").on("click", "button[name='pdf']",function(){

        var userName = $(this).siblings("form").attr("id").split("_")[1];
        var id = $(this).attr("id").split("_")[1];
        var dados = {
            USERNAME: "'" + userName + "'"
        };        

        // $.ajax({
        //     url: `http://localhost:8282/jasperserver/rest_v2/reports/reports/Imetame/Ficha_Usuario.pdf?USERNAME='${userName}'`,
        //     method: "GET",
        //     xhrFields: { responseType : 'blob' },
        //     beforeSend: function(xhr){
        //         xhr.setRequestHeader("Authorization", "Basic " + btoa('jasperadmin:jasperadmin'));   
        //         xhr.setRequestHeader("Accept", "application/pdf");             
        //     }
        // }).done(function(res){
        //     console.log(res);
        //     const fileUrl = URL.createObjectURL(res);
        //     window.open(fileUrl, '_blank');
        // }).fail(function(jqXhr, status){
        //     console.log(jqXhr)
        //     exibirMensagem("Erro", jqXhr.responseText, (jqXhr.status === 401) ? true : false);
        // });

        // window.open(`http://localhost:8282/jasperserver/rest_v2/reports/reports/Imetame/Ficha_Usuario.pdf?USERNAME='${userName}'`, '_blank');

        fetch("http://localhost:8282/jasperserver/rest_v2/reports/reports/Imetame/Ficha_Usuario.pdf?USERNAME='"+userName+"'",
            { 
                method: 'GET',
                headers: {
                    'Authorization': 'Basic ' + btoa("jasperadmin:jasperadmin")
                }
            }
        ).then(response => response.blob()).catch(erro => {
            console.log(erro);
        }).then(pdf => {
            const fileUrl = URL.createObjectURL(pdf);
            window.open(fileUrl, '_blank');
        });
    });

    
});