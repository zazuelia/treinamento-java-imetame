$(document).ready(function(){

    $("#login-form").submit(function(event){
        event.preventDefault();
        var params = {
            usuario: $("#email").val(),
            senha: $("#senha").val()
        };
        var form_method = $(this).attr("method");
        var form_action = $(this).attr("action");

        console.log(params, form_method, form_action);

        $.ajax({
            url: form_action,
            method: form_method,
            data: params,
            crossDomain: true,
            beforeSend: function(jqXhr) {
                console.log(jqXhr);
            }
        }).done(function(resposta, status, jqXHR){
            console.log(resposta);
            console.log(status);
            console.log(jqXHR.getAllResponseHeaders());
            console.log(decodeURIComponent(document.cookie));
            localStorage.setItem("token", jqXHR.getResponseHeader("Authorization"));
            localStorage.setItem("userName", params.usuario);
            window.location = "usuario.html";
        }).fail(function( jqXHR, textStatus ) {
            console.log(jqXHR, textStatus);
        });

        
    });
});