public class IncDec{

	public static void main(String[] args){
	
		int x = 10;
		int y = 20;
		int z = x++ * --y; // x = 10 | 19 | z = 190 
		System.out.println(x + " " + y + " " + z);
	}
}