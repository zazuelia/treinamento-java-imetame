import java.util.Arrays;
public class Teste {
	
	String nome;
	
	
	public void modificarNome(){
		
		String novoNome = " Elia";
		
		nome = nome + novoNome;
		
	}
	
	public Teste(){
		nome = "";
	}
	
	public Teste(String outroNome){
		
		nome = outroNome;
	}
	
	{
		int conta = 10;
		if(conta < 9){
			nome = "alguma coisa";
		} else{
			nome = "outra coisa";
		}	
		
	}
	
	public Teste(String outroNome, String sobreNome){
		nome = outroNome + " " + sobreNome;
	}
	
	public static void main(String[] args){
		
		System.out.println(args[0] + " " + args[1]);
		System.out.println(Arrays.asList(args));
		Teste teste = new Teste("Jose", " Cassani");
		Teste teste2 = teste;
		System.out.println("Nome Antes: " + teste.nome);
		teste.modificarNome();
		System.out.println("Nome Depois: " + teste2.nome);
	}
}