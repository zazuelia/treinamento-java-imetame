/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imetame.projeto.projetoimetame.util;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author jrobe
 */
public class Security {
    @Context UriInfo uriInfo;
    
    public String encodeText(String text) throws NoSuchAlgorithmException{
        
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(text.getBytes(StandardCharsets.UTF_8));
        
        return new String(Base64.getEncoder().encode(md.digest()));
    }
    
    public SecretKey generateScretKey(String secret) throws NoSuchAlgorithmException{
        
        String encodedString = encodeText(secret);
        return Keys.hmacShaKeyFor(encodedString.getBytes(StandardCharsets.UTF_8));
    }
    
    public String generateToken(String usuario, String senha) throws NoSuchAlgorithmException{
        
        String secret = "Tr3in@m3nt0J@v@!m3t@m3" + senha;
        SecretKey key = generateScretKey(secret);        
        
        String token = Jwts.builder()
                .setSubject(usuario)
                .setIssuer(uriInfo.getAbsolutePath().toString())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(LocalDateTime.now().plusMinutes(15).atZone(ZoneId.systemDefault()).toInstant()))
                .signWith(key, SignatureAlgorithm.HS256)
                .compact()
                ;
        
        return token;
    }
    
}
