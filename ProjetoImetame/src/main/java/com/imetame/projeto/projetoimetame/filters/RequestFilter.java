/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imetame.projeto.projetoimetame.filters;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.imetame.projeto.projetoimetame.entidades.SessionStore;
import com.imetame.projeto.projetoimetame.entidades.Usuario;
import com.imetame.projeto.projetoimetame.interfaces.Secure;
import com.imetame.projeto.projetoimetame.services.UsuarioServices;
import com.imetame.projeto.projetoimetame.util.Security;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.security.SignatureException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Priority;
import javax.crypto.SecretKey;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import org.glassfish.jersey.message.internal.ReaderWriter;

/**
 *
 * @author jrobe
 */
@Provider
@Secure
@RequestScoped
@Priority(Priorities.AUTHENTICATION)
public class RequestFilter implements ContainerRequestFilter{

    @Inject SessionStore session;
    @Inject Security security;
    @Inject UsuarioServices usuarioService;
    
    @Override
    public void filter(ContainerRequestContext context) throws IOException {
        
        String auth = new String();
        String secret = "Tr3in@m3nt0J@v@!m3t@m3";
        Gson gson = new Gson();
        JsonObject jsonBody = new JsonObject();
        
//        System.out.println("getEntityBody: " + getEntityBody(context));
        jsonBody = new JsonParser().parse(getEntityBody(context)).getAsJsonObject();        
        
        try {
            auth = context.getHeaderString(HttpHeaders.AUTHORIZATION).toString();
        } catch (Exception e) {
            throw new NotAuthorizedException(Response.status(Response.Status.UNAUTHORIZED).entity("Acesso Negado. Token inexistente").build());
            
        }
        
        if(!auth.startsWith("Bearer")){
            
            throw new NotAuthorizedException(Response.status(Response.Status.UNAUTHORIZED).entity("Acesso Negado. Tipo de token inválido").build());
        }
        
        String token = auth.substring("Bearer".length()).trim();
        
//        if(session.getPassword() == null){
//            throw new NotAuthorizedException(Response.status(Response.Status.UNAUTHORIZED).entity("Acesso Negado. Usuário não esta logado").build());
//        }

        Usuario usuario = usuarioService.getUsuarioByUserName(jsonBody.get("userName").getAsString());
        
        try {
            SecretKey secretKey = security.generateScretKey(secret + usuario.getPassword());
            
            try {
                Jws jws = Jwts.parserBuilder().setSigningKey(secretKey).build().parseClaimsJws(token);
                System.out.println("Body JWS: " + jws.getBody());
            } catch (ExpiredJwtException | MalformedJwtException | UnsupportedJwtException | SignatureException | IllegalArgumentException e) {
                throw new NotAuthorizedException(Response.status(Response.Status.UNAUTHORIZED).entity("Acesso Negado. Exceção no token: " + e.getMessage()).build());
            }
        } catch (NoSuchAlgorithmException ex) {
            throw new InternalError("Erro na geracao da secret key: " + ex.getMessage());
        }
    }
    
//    private String getEntityBody(ContainerRequestContext reqContext) throws IOException{
//        
//        byte[] buffer = new byte[1024];
//        InputStream in = reqContext.getEntityStream();
//        StringBuilder sb = new StringBuilder();
//        int charsRead;
//        Reader reader = new InputStreamReader(in, StandardCharsets.UTF_8);
//        
//        while((charsRead = in.read(buffer, 0, buffer.length)) > 0) {
//            sb.append(new String(buffer, StandardCharsets.UTF_8)).append("\n");
//        }
//        
//        reqContext.setEntityStream(new ByteArrayInputStream(buffer));
//        
//        return sb.toString();
//    }
    
    private String getEntityBody(ContainerRequestContext requestContext) 
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        InputStream in = requestContext.getEntityStream();
         
        final StringBuilder b = new StringBuilder();
        try
        {
            ReaderWriter.writeTo(in, out);
 
            byte[] requestEntity = out.toByteArray();
            b.append(new String(requestEntity)).append("\n");

            requestContext.setEntityStream( new ByteArrayInputStream(requestEntity) );
 
        } catch (IOException ex) {
            System.out.println("getEntityBody IOException: " + ex.getMessage());
        }
        return b.toString();
    }
    
}
