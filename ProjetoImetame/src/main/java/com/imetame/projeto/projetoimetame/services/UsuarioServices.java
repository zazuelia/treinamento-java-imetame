/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imetame.projeto.projetoimetame.services;

import com.imetame.projeto.projetoimetame.entidades.SessionStore;
import com.imetame.projeto.projetoimetame.entidades.Usuario;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author jrobe
 */
public class UsuarioServices {
    
    @Inject SessionStore session;
    
    EntityManagerFactory emf;
    EntityManager em;
    
    /**
     * Metodo para criar as instancias das Entities Managers
     */
    private void criaInstanciaEntityManager(){
        
        emf = Persistence.createEntityManagerFactory("imetame_pu");
        em = emf.createEntityManager();
    }
    
    /**
     * Metodo para fechar as instancias das Entities Managers
     */
    private void fechaInstanciaEntityManager(){
        
        em.close();
        emf.close();
    }
    
    /**
     * Metodo para Inserir/Atualizar Usuario
     * @param usuario
     * @throws SQLException 
     */
    public Usuario insertUpdateUsuario(Usuario usuario) throws SQLException{
        
        // Chama o metodo para criar as intanceias das Entities Managers
        criaInstanciaEntityManager();
        
        // Inicia a transacao
        em.getTransaction().begin();
        
        try {
            // Acopla o Usuario na transacao. Durante a acoplagem o objeto é persistido e 
            // o mesmo será criado (caso não existir) ou atualizado (caso existir)
            em.merge(usuario);
//            System.out.println(Arrays.asList(em.merge(usuario)));
        } catch (RuntimeException e) {
            System.out.println("inserUpdateUsuario: " + e.getMessage());
        }
        
        // Faz o commit da transacao
        em.getTransaction().commit();
        
        // Chama o metodo para fechar as instancias das Entities Managers
        fechaInstanciaEntityManager();
        
        return usuario;
    }
    
    /**
     * Metodo para resgatar usuario pelo ID
     * @param id
     * @return Usuario
     */
    public Usuario getUsuarioById(int id){
        
        // Chama o metodo para criar as intanceias das Entities Managers
        criaInstanciaEntityManager();
        
        // Cria StringBuilder para definir a query
        StringBuilder query = new StringBuilder();
        query.append("SELECT * FROM usuario WHERE id = ").append(id);
        
        // Regata o Usuario 
        Usuario usuario = (Usuario)em.createNativeQuery(query.toString(), Usuario.class).getSingleResult();
        
        // Chama o metodo para fechar as instancias das Entities Managers
        fechaInstanciaEntityManager();
        
        return usuario;
    }
    
    public Usuario getUsuarioByUserName(String userName) {
        
        criaInstanciaEntityManager();
        
        // Cria StringBuilder para definir a query
        StringBuilder query = new StringBuilder();
        query.append("SELECT * FROM usuario WHERE user_name = '").append(userName).append("'");
        
        // Regata o Usuario 
        Usuario usuario = (Usuario)em.createNativeQuery(query.toString(), Usuario.class).getSingleResult();
        
        // Chama o metodo para fechar as instancias das Entities Managers
        fechaInstanciaEntityManager();
        
        return usuario;
    }
    
    /**
     * Metodo para elimiar Usuario pelo ID
     * @param id 
     */
    public void excluirUsuarioById(int id){
        
        // Buscar o usuario pelo ID (reaproveita o metodo da classe corrente)
        Usuario usuario = getUsuarioById(id);
        
        // Chama o metodo para criar as intanceias das Entities Managers
        criaInstanciaEntityManager();        
        
        // Reatribui o Usuario acoplado na transacao
        usuario = (Usuario)em.merge(usuario);
        
        // Inicia a transacao
        em.getTransaction().begin();
        
        // Remove o Usuario
        em.remove(usuario);
        
        // Faz o commit da transcao
        em.getTransaction().commit();

        // Chama o metodo para fechar as instancias das Entities Managers
        fechaInstanciaEntityManager();
        
        
    }
    
    /**
     * Metodo para retornar Lista de Usuario
     * @return List<Usuario>
     */
    public List<Usuario> getUsuarios(){
        
        // Chama o metodo para criar as intanceias das Entities Managers
        criaInstanciaEntityManager();
        
        StringBuilder query = new StringBuilder();
        query.append("SELECT * FROM usuario");
        
        // Cria a lista e executa a query atribuindo o resultado como lista de Usuario
        List<Usuario> listaUsuario = (List<Usuario>)em.createNativeQuery(query.toString(), Usuario.class).getResultList();
        
        
        // Retorna a lista de Usuario
        return listaUsuario;
    }
    
    public void autenticaUsuario(String usuario, String senha) throws Exception{
        
        criaInstanciaEntityManager();
        StringBuilder query = new StringBuilder();
        query.append("SELECT * FROM usuario WHERE user_name = '")
                .append(usuario).append("' AND password = '").append(senha).append("'");
        
        System.out.println("query: " + query.toString());
        
        try {
            Usuario user = (Usuario)em.createNativeQuery(query.toString(), Usuario.class).getSingleResult();
            session.setUsuario(user.getUser_name());
            session.setPassword(user.getPassword());
        } catch (Exception e) {
            throw new Exception("Usuário e/ou Senha inválido(s)! " + e.getMessage());
        }
        
        fechaInstanciaEntityManager();
        
    }
    
    public int getLastId() {
        
        criaInstanciaEntityManager();
        StringBuilder query = new StringBuilder();
        query.append("SELECT MAX(id) FROM usuario");
        
        int lastId = (Integer)em.createNativeQuery(query.toString()).getSingleResult();
        
        System.out.println("Last ID: " + lastId);
        
        fechaInstanciaEntityManager();
        
        return lastId;
    }
    
}
