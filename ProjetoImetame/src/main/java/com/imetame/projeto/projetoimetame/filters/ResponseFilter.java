/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imetame.projeto.projetoimetame.filters;

import java.io.IOException;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author jrobe
 */
@Provider
@SessionScoped
public class ResponseFilter implements ContainerResponseFilter, Serializable{

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        
            System.out.println("ResponseFilter: " + requestContext.getHeaders().toString());
            responseContext.getHeaders().add("Access-Control-Allow-Origin", "http://localhost:5500");
            responseContext.getHeaders().add("Access-Control-Allow-Headers", "Authorization, origin, content-type");
            responseContext.getHeaders().add("Access-Control-Allow-Methods", "*");
            responseContext.getHeaders().add("Access-Control-Expose-Headers", "Authorization");
            responseContext.getHeaders().add("Access-Control-Allow-Credentials", "true");
    }    
}
