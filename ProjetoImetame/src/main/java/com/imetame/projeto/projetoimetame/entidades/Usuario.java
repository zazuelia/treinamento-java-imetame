/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imetame.projeto.projetoimetame.entidades;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author jrobe
 */
@Entity
public class Usuario implements Serializable{
    
    @Id
    private int id;
    
    @Column(name = "user_name")
    private String usuario;
    private String name;
    private String password;
    private String telefone;
    private String email;
    private int ativo;
    private String endereco;
    private String complemento;
    private String cidade;
    private String estado;
    private String cep;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_name() {
        return usuario;
    }

    public void setUser_name(String user_name) {
        this.usuario = user_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAtivo() {
        return ativo;
    }

    public void setAtivo(int ativo) {
        this.ativo = ativo;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }
    
    @Override
    public String toString(){
        
        StringBuilder sb = new StringBuilder();
        sb.append("[")
                .append(this.usuario)
                .append(",")
                .append(this.name)
                .append(",")
                .append(this.password)
                .append(",")
                .append(this.telefone)
                .append(",")
                .append(this.email)
                .append(",")
                .append(this.ativo)
                .append(",")
                .append(this.endereco)
                .append(",")
                .append(this.complemento)
                .append(",")
                .append(this.cidade)
                .append(",")
                .append(this.estado)
                .append(",")
                .append(this.cep)
                .append("]");
        return sb.toString();
    }
    
}
