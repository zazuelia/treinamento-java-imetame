package com.imetame.projeto.projetoimetame.resources;

import com.imetame.projeto.projetoimetame.entidades.Usuario;
import com.imetame.projeto.projetoimetame.services.ConexaoTeste;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author 
 */
@Path("javaee8")
public class JavaEE8Resource {
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)    
    public Response ping(Usuario usuario){
        
        System.out.println(usuario);
        ConexaoTeste conexao = new ConexaoTeste();
        conexao.criaUsuario(usuario);
        return Response
                .ok("ping 123")
                .build();
    }
}
