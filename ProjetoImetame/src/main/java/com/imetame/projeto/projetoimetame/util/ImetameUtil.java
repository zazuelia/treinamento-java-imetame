/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imetame.projeto.projetoimetame.util;

import com.google.gson.JsonObject;

/**
 *
 * @author jrobe
 */
public class ImetameUtil {
    
    public static final String SERVICE_RESPOSE_OK = "OK";
    public static final String SERVICE_REPOSE_ERRO = "ERRO";
    
    public static JsonObject criaServiceResponse(String response, String message){
        
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("ServiceResponse", response);
        jsonObj.addProperty("Message", message);
        
        return jsonObj;
    }
}
