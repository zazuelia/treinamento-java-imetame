/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imetame.projeto.projetoimetame.resources;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.imetame.projeto.projetoimetame.entidades.BodyRequest;
import com.imetame.projeto.projetoimetame.entidades.Usuario;
import com.imetame.projeto.projetoimetame.interfaces.Secure;
import com.imetame.projeto.projetoimetame.services.UsuarioServices;
import com.imetame.projeto.projetoimetame.util.ImetameUtil;
import com.imetame.projeto.projetoimetame.util.Security;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

/**
 *
 * @author jrobe
 */
@Path("usuarios")
public class UsuariosResource {
    
    @Inject UsuarioServices services;
    @Inject Security security;
    JsonObject jsonObj;
    Gson gson;
    
    @Path("incluir-alterar-usuario")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Secure
    public Response incluirUsuario(Usuario usuario){
        
        Gson gson = new Gson();
        
        try {
            
            // Se não possuir Id então é uma inclusão, senão alteração
            if(usuario.getId() == 0) {
                //Busca o ultimo Id e atribuiu ao usuario
                usuario.setId(services.getLastId() + 1);
            }
            
            // Chamada do Servico para Incluir/Alterar o Usuario            
            jsonObj = ImetameUtil.criaServiceResponse(ImetameUtil.SERVICE_RESPOSE_OK, gson.toJson(services.insertUpdateUsuario(usuario)));
        } catch (SQLException ex) {
            jsonObj = ImetameUtil.criaServiceResponse(ImetameUtil.SERVICE_REPOSE_ERRO, "[Erro Inclusao Usuario] " + ex.getMessage());
        }
        
        return Response.ok(jsonObj.toString()).build();
    }
    
    @Path("apagar-usuario/id")
    @POST
    @Secure
    public Response apagarUsuarioById(Usuario usuario){
        
        try {
            // Chamada do Servico para Eliminar o Usuario
            services.excluirUsuarioById(usuario.getId());
            jsonObj = ImetameUtil.criaServiceResponse(ImetameUtil.SERVICE_RESPOSE_OK, "Usuario apagado com sucesso!");
        } catch (Exception e) {
            jsonObj = ImetameUtil.criaServiceResponse(ImetameUtil.SERVICE_REPOSE_ERRO, e.getMessage());
        }
        return Response.ok(jsonObj.toString()).build();
    }
    
    @Path("apagar-usuario/user-name/{user-name}")
    @GET
    @Secure
    public Response apagarUsuarioByUserName(@PathParam("user-name") String userName){
        
        
        return Response.ok().build();
    }
    
    @Path("consultar-usuario-ativo/id/{id}")
    @GET
    @Secure
    public Response getUsuarioById(@PathParam("id") int id){
        
        Usuario usuario;
        Gson gson = new Gson();
        
        try {
            // Chamada do Servico para Resgatar o Usuario pelo Id
            usuario = services.getUsuarioById(id);
            jsonObj = ImetameUtil.criaServiceResponse(ImetameUtil.SERVICE_RESPOSE_OK, gson.toJson(usuario));
        } catch (Exception e) {
            jsonObj = ImetameUtil.criaServiceResponse(ImetameUtil.SERVICE_REPOSE_ERRO, e.getMessage());
        }
        
        return Response.ok(jsonObj.toString()).build();
    }
    
    @Path("consultar-usuario-ativo/user-name/{id}")
    @GET
    @Secure
    public Response getUsuarioByUserName(@PathParam("user-name") String userName){
        
        return Response.ok().build();
    }
    
    @Path("consultar-usuario-ativo")
    @Consumes(MediaType.APPLICATION_JSON)
    @POST
    @Secure
    public Response getListaUsuariosAtivos(BodyRequest body){
        
        List<Usuario> listaUsuario;
        gson = new Gson();
        try {
            listaUsuario = services.getUsuarios();
            jsonObj = ImetameUtil.criaServiceResponse(ImetameUtil.SERVICE_RESPOSE_OK, gson.toJson(listaUsuario));
        } catch (Exception e) {
            jsonObj = ImetameUtil.criaServiceResponse(ImetameUtil.SERVICE_REPOSE_ERRO, e.getMessage());
        }
        return Response.ok(jsonObj.toString()).build();
    }
    
    @Path("login-usuario")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response loginUsuario(@FormParam("usuario") String usuario, @FormParam("senha") String senha) throws NoSuchAlgorithmException {
        
        try {
            services.autenticaUsuario(usuario, senha);
            NewCookie cookie = new NewCookie("session", security.encodeText(usuario + senha));
            return Response.ok().header(HttpHeaders.AUTHORIZATION, security.generateToken(usuario, senha)).build();
        } catch (Exception e) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).build();
        }       
        
    }
    
}
