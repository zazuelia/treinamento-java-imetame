/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.imetame.projeto.projetoimetame.services;

import com.imetame.projeto.projetoimetame.entidades.Usuario;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author jrobe
 */
public class ConexaoTeste {
    
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("imetame_pu");
    
    public void criaUsuario(Usuario usuario){        
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        em.persist(usuario);
        em.getTransaction().commit();

//        String query = "SELECT * FROM usuario";
//        List<Usuario> usuarios = (List<Usuario>)em.createNativeQuery(query, Usuario.class).getResultList();
//        
//        System.out.println("Usuarios: " + usuarios);
        
        
        em.close();
        emf.close();
    }
    
}
